let lang
import { subLang } from './lang/lang.js'
subLang.subscribe(v => lang = v)

import { writable } from 'svelte/store'

let def = {
  none:{open:false,slugs:{lat:'',eng:'',rus:''}}, // home route
  faq:{open:false,slugs:{lat:'jautajumi',eng:'jautajumi',rus:'jautajumi'}},
  privacy:{open:false,slugs:{lat:'apstrade',eng:'apstrade',rus:'apstrade'}},
  recycling:{open:false,slugs:{lat:'skirosana',eng:'skirosana',rus:'skirosana'}},
  lottery:{open:false,slugs:{lat:'jaunumi',eng:'jaunumi',rus:'jaunumi'}},
}

const openNavigation = name => {
  console.log(`Opening navigation:`,name)
  for(let i in def) def[i].open = false // toggle off all other events
  def[ name ].open = !def[ name ].open

  console.log( def )

  navigation.set( def )
  hasOpenCheck()
}

export const hasOpenCheck = () => {
  let isItOpen = (def.faq.open || def.privacy.open || def.recycling.open || def.lottery.open)
  hasOpen.set( isItOpen )
}

export const toggleNavigation = name => {
  let hashLang = location.hash.substr(1).split('/')[0]
  hashLang = hashLang == '' ? 'lat' : hashLang
  location.hash = '#'+hashLang+'/'+def[name].slugs[ hashLang ]
  openNavigation( name )
}

export let navigation = writable( def )
export let hasOpen = writable( false )



// navigation
let hash = location.hash.substr(1)
if( hash.indexOf('?') !== -1 ) hash = hash.substr(0,hash.indexOf('?'))
let openedCategory = hash.split('/')[1]
for( let i in def ){
  if( def[i].slugs[lang.lang] == openedCategory ) openNavigation(i)
}
