export default {
  'lang':'eng',

  'start':'Home',

  // header
  'news':'News',
  'faq':'Questions',
  'contacts':'Contacts',
  'contractSamples':'Samples of agreement',
  'privatePerson':'For natural persons',
  'legalPerson':'For legal entities',

  'makeadeal':'Sign agreement',

  'callme':'<b>Phone:</b><br />67847844,<br />1848*',

  'setLangLAT':'LAT',
  'setLangRUS':'RUS',
  'setLangENG':'ENG',

  'faqTitle':'QUESTIONS AND ANSWERS<br />Introduction of the Tīrīga AS household waste management system in the city of Riga',

  // intro
  'ourcity':'Our city<br />is',
  'obey':'We invite you to switch your contract promptly so that Tīrīga can take care of your city.',

  "title":"Riga is getting<br />cleaner!",
  "intro":'Tīrīga AS was founded, under the auspices of public-private partnership, in order to  carry out the modernisation of  the waste management system in Riga and to introduce a modern, efficient waste sorting system and solutions that are convenient for residents, including developing infrastructure and giving residents the chance to sort waste. Provision of household waste management services in Riga will start on 15 September 2019, within the framework of a public-private partnership, in accordance with the results of the municipality’s public procurement.<br />Waste management agreements must be switched by all of Riga\'s residents including both natural persons and legal entities.',
  "gotext":'You switch your agreement today and we at<br />“Tīrīga” AS will do the rest!',

  'actionChange':'SWITCH YOUR AGREEMENT',

  // why
  'whythis':'Why Tīrīga?',
  'whyintro':'The time has come to modernize Riga\'s household waste management! Registered by SIA Getļiņi EKO and SIA CREB Rīga, Tīrīga AS is launching the City of Riga\'s biggest waste management modernization project with a single development goal – to clean up Riga.',
  'whydesc':'',

  // recycling
  recycling:{
    'title':'How to sort properly?',
    'menu':'How to sort properly?',
    household:{
      'h2':'Household waste',
      'h3':'Not to be disposed:',
      'li1':'<li>- bulky waste</li>'+
            '<li>- tyres</li>'+
            '<li>- bulbs and batteries</li>'+
            '<li>- construction and demolition waste</li>'+
            '<li>- electrical devices</li>',
      'li2':'<li>- medical waste</li>'+
            '<li>- hot ashes</li>'+
            '<li>- radioactive substances</li>'+
            '<li>- liquid waste</li>',

    },
    paper:{
      'h2':'Paper, plastic and metal',
      'h3':'Not to be disposed:',
      'p':'Drinks bottles and hard plastic bottles, jugs, boxes, bags, film, paper and cardboard packaging, wastepaper, tetra packs, metal boxes of stationary and plastic covers, cans, household metal objects',
      'li1':'<li>- dirty, wet and non-flattened packaging</li>'+
            '<li>- foam plastic</li>'+
            '<li>- laminated paper</li>'+
            '<li>- single-use dishes</li>',
      'li2':'<li>- plastic household supplies</li>'+
            '<li>- gas cylinders</li>'+
            '<li>- hairsprays and deodorants</li>'+
            '<li>- batteries</li>',
    },
    glass:{
      'h2':'Glass',
      'h3':'Not to be disposed:',
      'p':'Glass bottles and jars',
      'li1':'<li>- dirty glass packaging with<br>&nbsp;&nbsp;food leftovers</li>'+
            '<li>- window and car glass</li>'+
            '<li>- glass dishes</li>',
      'li2':'<li>- bulbs</li>'+
            '<li>- parfum flasks</li>'+
            '<li>- cosmetic jars</li>'+
            '<li>- mirrors</li>',
    },
  },

  // lottery
  lottery:{
    'title':'Kas laicīgi līgumu pārslēdz, tas <dust>laimē</dust> elektro-skūteri!',
    'desc':'Ikviens, kurš pārslēdz līgumu par atkritumu apsaimniekošanu ar A/S Tīrīga no 2019. gada 5. augusta līdz 2019. gada 30. augustam, piedalās loterijā.',
    'rules':'Noteikumi',
    'prize':'Balvas - trīs elektroskūteri Xiaomi',
    'legal':'Loterijas atļaujas Nr. 5878',
  },

  // changes
  changes:{
    'title':'What changes will there be?',
    left:{
      'title':'New <dust>tariffs</dust>',
      'now':'Currently',
      'lines':[
        'Container and vehicle fleet provision, transport costs',
        'Formation of a separately collected waste collection system, site construction',
        'Waste management costs',
        'Waste preparation for landfill, landfill and Natural Resources Tax (NRT)',
        'Value Added Tax (VAT)',
      ],
      'total':'TOTAL:',
    },
    right:{
      'title':'New <dust>benefits</dust>',
      'list':[
        'One price - the same conditions throughout the city.',
        'Efficient logistics - one route, one road.',
        'The chance to influence the size of your bills, sorting waste.',
      ],
    },
  },

  // Goals
  goals:{
    title:'What is Tīrīga\'s goal?',
    list:[
      {title:'Tīrīga<br />thinking',desc:'To educate and invite everyone to take part in cleaning up the city, consciously thinking about a clean environment everyday'},
      {title:'Tīrīga<br />action',desc:'To follow global examples and introduce contemporary solutions to waste management infrastructure'},
      {title:'A Tīrīga<br />city',desc:'As a result of goal-oriented thinking and action, we want to join forces to make our city better'},
    ],
  },

  // Timeline
  timeline:{
    title:'How will we change the management procedure and make the environment cleaner?',
    desc:'Every goal requires reference points. Ours are as follows.',
    list:[
      {title:'15 July 2019',desc:'Tīrīga starts to switch agreements with Riga\'s residents'},
      {title:'15 September 2019',desc:'Tīrīga starts to provide services to the city\'s residents'},
      {title:'January 2020',desc:'Introduction of new waste containers in the city'},
      {title:'April 2020',desc:'Creation of new waste collection sites'},
      {title:'March 2020',desc:'New environmentally friendly vehicles will enter service'},
    ],
  },

  // middle part
  'contractSample':'Sample&nbsp;of&nbsp;agreement',
  'info':'How does this apply to you?',
  's1title':'If you own a private house',
  's1text':'You have study the new waste management procedures and sign a new waste management agreement.',
  's2title':'If you are an administrator',
  's2text':'You have to arrange the signing of a new waste management agreement, as well as study the management procedures of Tīrīga AS and inform the house’s residents about this. ',
  's3title':'You own or rent an apartment',
  's3text':'If you are an apartment owner or are renting an apartment, you do not have to sign the contract yourself. The manager of your building will sign the contract on your behalf.',
  's4title':'If you are a legal entity',
  's4text':'You have to arrange the signing of a new waste management agreement, as well as study the management procedures of Tīrīga AS and inform the house’s tenants about this. ',

  'makeDeal':'Example of the contract',

  // footer
  'gofooter':'Renew your waste<br />management agreement! ',

  'btnCleanR':'For Clean R customers ',
  'btnEcoBaltiaVide':'For Eco Baltia Vide customers',
  'btnNewClients':'For new customers',

  'footer':'Processing of personal data',

  'contactsTitle':'Tīrīga AS customer<br />service centres: ',
  'contactsAdress1':'Pepsi Bowling Sports and Leisure Centre,<br />Uzvaras Boulevard 10',
  'contactsAdress2':'The Visitors Reception Centre of the Riga City Council,<br />1st Floor, Brīvības Street 49/53',
  'contactsWorkdays1':'Weekdays: 8.00 – 18.00',
  'contactsWeekend1':'Saturdays: 10.00 – 15.00',
  'contactsWorkdays2':'Weekdays: 8.00 – 19.00',
  'contactsWeekend2':'Saturdays: 10.00 – 15.00',
  'contactsPhoneStr':'Phone',
  'contactsPhoneNr':'1848*, 67847844',
  'contactsNotice':'Charge according to the tariffs of the operator',
  'contactsEmailStr':'E-mail',
  'contactsEmail':'klienti@tiriga.lv',
  'contactsComp':'”Tīrīga” AS',
  'contactsRegNr':'Reg. No. 40203215318',
  'contactsLegal':'Legal address: Vietalvas iela 5A, Rīga, LV-1009',
  'contactsPostal':'Postal address: Vietalvas iela 5A, Rīga, LV-1009',
  'contactsBank':'Bank: A/S Luminor bank',
  'contactsBankCode':'Code: RIKOLV2X',
  'contactsBankAcc':'Acc. No. LV50RIKO0002930275309',

  'footerContacts':'<strong>Contacts for media:</strong><br />Maija Dulle-Sūniņa, Communication Consultant<br />Mob. tel. + 371 28825258<br />E-mail: <a href="mailto:maija.dulle@mrsgrupa.lv" target="_blank">maija.dulle@mrsgrupa.lv</a>',

  'cookies':'We use cookies, in order to provide optimal usage experience and functionality of the website. By pressing the button “I agree”, you declare that you agree to use cookies according to our Privacy policy.',
  'cookiesAgree':'Agree',
}
